<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("app_permissions",function (Blueprint $table){
            $table->integer("id",true);
            $table->string("description");
            $table->string("action");
            $table->string("author_id");
            $table->foreign("author_id")->references("auuid")->on("users");            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("app_permissions");        
    }
}
