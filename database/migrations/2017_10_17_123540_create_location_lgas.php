<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationLgas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("location_lgas",function( Blueprint $table){
            $table->integer("id",true);
            $table->string("name");
            $table->string("lga_code");
            $table->integer("area_id");
           $table->foreign("area_id")->references("id")->on("location_areas");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("location_lgas");        
    }
}
