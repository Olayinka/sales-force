<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_roles",function(Blueprint $table){
            $table->integer("id",true);
            $table->string('user_auuid');
            $table->integer('role_id');
           $table->foreign('user_auuid')->references('auuid')->on('users');
           $table->foreign('role_id')->references('id')->on('app_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("user_roles");        
    }
}
