<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCountry extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'location_country';
}
