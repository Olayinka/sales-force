<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>

  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
<meta name="robots" content="noindex">

<!-- Material Design Icons  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- Roboto Web Font -->
<link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

<!-- MDK -->
<link type="text/css" href="css/material-design-kit.css" rel="stylesheet">

<!-- Sidebar Collapse -->
<link type="text/css" href="css/sidebar-collapse.css" rel="stylesheet">

<!-- App CSS -->
<link type="text/css" href="css/style.css" rel="stylesheet">
@yield('head')
</head>
<body class="top-navbar">

  <!-- Navbar -->
<nav class="navbar navbar-dark bg-primary navbar-full navbar-fixed-top">

	<!-- Toggle sidebar -->
	<button class="navbar-toggler" type="button" data-toggle="sidebar"></button>

	<!-- Brand -->
	<a href="student-dashboard.html" class="navbar-brand"><i class="material-icons">school</i> LearnPlus</a>
	
	<!-- Search -->
<form class="navbar-search-form hidden-sm-down">
  <input type="text" class="form-control" placeholder="Search">
  <button class="btn" type="button"><i class="material-icons">search</i></button>
</form>
<!-- // END Search -->

	<div class="navbar-spacer"></div>
	
	<!-- Menu --> 
	<ul class="nav navbar-nav hidden-sm-down">
		<li class="nav-item">
			<a class="nav-link" href="student-forum.html">Forum</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="student-help-center.html">Get Help</a>
		</li>
	</ul>
	
	<!-- Menu -->
	<ul class="nav navbar-nav">

		<li class="nav-item">
			<a href="student-cart.html" class="nav-link">
				<i class="material-icons">shopping_cart</i>
			</a>
		</li>
		<li class="nav-item">
			<a href="student-cart.html" class="nav-link">
			Adekunle
			</a>
		</li>
		
		<!-- User dropdown -->
<li class="nav-item dropdown">
  <a class="nav-link active dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"><img src="images/guy-6.jpg" alt="Avatar" class="rounded-circle" width="40"></a>
  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
    <a class="dropdown-item" href="student-account-edit.html">
      <i class="material-icons">edit</i> Edit Account
    </a>
    <a class="dropdown-item" href="student-profile.html">
      <i class="material-icons">person</i> Public Profile
    </a>
    <a class="dropdown-item" href="guest-login.html">
      <i class="material-icons">lock</i> Logout
    </a>
  </div>
</li>
<!-- // END User dropdown -->

	</ul>
	<!-- // END Menu -->

</nav>
<!-- // END Navbar -->

<!--Insert Content -->

@yield('content')

<!--End Content -->

@yield('footer')

  <!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap -->
<script src="js/tether.js"></script>
<script src="js/bootstrap.js"></script>

<!-- MDK -->
<script src="js/dom-factory.js"></script>
<script src="js/material-design-kit.js"></script>

<!-- Sidebar Collapse -->
<script src="js/sidebar-collapse.js"></script>

<!-- App JS -->
<script src="js/main.js"></script>
</body>
</html>
