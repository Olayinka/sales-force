<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("users_profile",function (Blueprint $table){
            $table->string("auuid");
            $table->primary("auuid");
            $table->string('name');
            $table->string("profile_picture");
            $table->string("phone_number");
           $table->foreign("auuid")->references("auuid")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("users_profile");        
    }
}
