<?php 

namespace App\Http\Controllers;

use Illuminate\App\Request;

use App\Http\Requests;

class LoginController extends Controller{

    public function index(){
        $title = "SalesForce | Login";
        return view('login')->with(['title'=>$title]);
    }
}

