<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationTerritory extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'location_territories';
}
