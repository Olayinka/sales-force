<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationSites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("location_sites",function(Blueprint $table){
            $table->integer("id",true);
            $table->string("site_id");
            $table->string("address");
            $table->string("town_name");
            $table->string("site_code");
            $table->boolean("is_active")->default(true);
            $table->integer("territory_id");
            $table->string("site_class_code");
            $table->string("latitude");
            $table->string("longitude");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("location_sites");        
    }
}
