<?php 

namespace App\Http\Controllers;

use Illuminate\App\Request;

use App\Http\Requests;

class DashboardController extends Controller{

    public function index(){
        $title = "SalesForce | Dashboard";
        $page = "Dashboard";
        return view('home')->with(['title'=>$title,"page"=>$page]);
    }
}

