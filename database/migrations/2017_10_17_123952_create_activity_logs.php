<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("activity_logs",function(Blueprint $table){
            $table->integer("id",true);
            $table->string("user_auuid");
            $table->string("content_type_id");
            $table->string("object_id");
            $table->string("object_repr");
            $table->datetime("action_time");
            $table->smallInteger("action_flag");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("activity_logs");        
    }
}
