<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationArea extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'location_areas';
}
