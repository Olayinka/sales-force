<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('auuid');
            $table->primary('auuid');
            $table->string('email')->unique();
            $table->string('password_hash');
            $table->boolean('is_deactivated')->default(false);
            $table->boolean('is_locked_out')->default(false);
            $table->timestamp('last_login_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');        
    }
}
